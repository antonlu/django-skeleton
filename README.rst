=====
MODULE
=====

MODULE is a Django app to XXX, with a responsive web design.

Detailed documentation is in the "docs" directory.

Quick start
-----------

1. Add "MODULE" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'MODULE.apps.MODULE_CONFIG',
    ]

2. Include the cookbook URLconf in your project urls.py like this::

    path('MODULE_URL/', include('MODULE.urls'))

3. Run ``python manage.py migrate`` to create the MODULE models.

4. Start the development server and visit http://127.0.0.1:8000/admin/
   to create recipes, or upload them on the index page.

5. Visit http://127.0.0.1:8000/MODULE_URL/ to view the recipes.