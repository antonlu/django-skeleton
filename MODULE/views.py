import logging
from django.views import generic

log = logging.getLogger(__name__)


# Create your views here.
class IndexView(generic.ListView):
    template_name = 'MODULE/index.html'
    model = None
    context_object_name = 'a_name'

    def get_queryset(self):
        return None
