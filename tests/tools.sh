#!/bin/bash

BEGIN="# > BEGIN MODULE"
END="# > END MODULE"

init () {
	if [[ ! -z $SETTINGSPY ]]; then
		sed -i "/^INSTALLED_APPS = \[/a\ \ \ \ 'MODULE.apps.MODULE_CONFIG'," $SETTINGSPY
	fi

	if [[ ! -z $URLSPY ]]; then
		sed -i "/^urlpatterns = \[/a\ \ \ \ path('MODULE/', include('MODULE.urls'))," $URLSPY
	fi

	if [[ ! -z $DOCKERFILE ]]; then
		cat <<EOF | sed -i "/^\# BEGIN SUBMODULES/r /dev/stdin" $DOCKERFILE
$BEGIN
ENV PYTHONPATH $repodir:$PYTHONPATH
$END
EOF
	fi

	if [[ ! -z $DOCKERCOMPOSE ]]; then
		cat <<EOF | sed -i "/\# BEGIN SUBMODULES/r /dev/stdin" $DOCKERCOMPOSE
$BEGIN
pip install -e $repodir
$END
EOF
	fi
}

remove () {
	if [[ ! -z $SETTINGSPY ]]; then
		sed -i "/MODULE_CONFIG/d" $SETTINGSPY
	fi

	if [[ ! -z $URLSPY ]]; then
		sed -i "/MODULE.urls/d" $URLSPY
	fi

	if [[ ! -z $DOCKERFILE ]]; then
		sed -i "/$BEGIN/,/$END/d" $DOCKERFILE
	fi


	if [[ ! -z $DOCKERCOMPOSE ]]; then
		sed -i "/$BEGIN/,/$END/d" $DOCKERCOMPOSE
	fi
}
